package com.notemaker.app.activities.ui.fragments.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import android.util.Log.w
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast

import com.notemaker.app.R
import com.notemaker.app.activities.ui.fragments.activities.AuthenticationActivity
import com.notemaker.app.activities.ui.fragments.activities.WorkingDashboard
import com.notemaker.app.activities.sharedpreferences.UserData
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.authentication_activity.*
import kotlinx.android.synthetic.main.fragment_sign_in.view.*


class SignInFragment : BaseAuthFragment() {

    private lateinit var auth: FirebaseAuth
    private lateinit var activityContext: AuthenticationActivity
    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        readUserData()
        init()

    }

    override fun getLayoutResource(): Int = R.layout.fragment_sign_in

    private fun init() {

        rootView!!.signInButton.setOnClickListener { signIn() }
        rootView!!.signUpButtonSuggestion.setOnClickListener {  signUpSuggest()}

    }
    private fun signUpSuggest(){
        activityContext = activity as AuthenticationActivity
        val mPager = activityContext.authenticationVP
        if (mPager.currentItem == 0)
            mPager.currentItem = 1
    }

    private fun signIn() {

        val email = rootView!!.emailEditTextSI.text.toString()
        val password = rootView!!.passwordEditTextSI.text.toString()
        activityContext = activity as AuthenticationActivity
        if (email.isNotEmpty() && password.isNotEmpty()) {
            auth = FirebaseAuth.getInstance()
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(activityContext) { task ->
                    if (task.isSuccessful) {
                        saveUserData()
                        val intent = Intent(
                            activityContext,
                            WorkingDashboard::class.java
                        )
                        startActivity(intent)
                        activityContext.finish()
                        d("success", "signInWithEmail:success")
                    } else {
                        w("", "signInWithEmail:failure", task.exception)
                        Toast.makeText(
                            activityContext, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                }
        } else {
            Toast.makeText(
                activityContext, "Please fill all fields",
                Toast.LENGTH_SHORT
            ).show()
        }
    }


    private fun saveUserData() {
        UserData.instance().saveData("UserEmail", rootView!!.emailEditTextSI.text.toString())
        UserData.instance().saveData("UserPassword", rootView!!.passwordEditTextSI.text.toString())

    }


    private fun readUserData() {
        val email = UserData.instance().readData("UserEmail")
        val password = UserData.instance().readData("UserPassword")
        rootView!!.emailEditTextSI.setText(email)
        rootView!!.passwordEditTextSI.setText(password)
        d("email", email.toString())


    }
}
