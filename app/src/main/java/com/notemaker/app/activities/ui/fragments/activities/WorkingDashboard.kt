package com.notemaker.app.activities.ui.fragments.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log.d

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.notemaker.app.R
import com.notemaker.app.activities.ui.fragments.adapters.NotesRecyclerViewAdapter

import com.notemaker.app.activities.room.AppDatabase
import com.notemaker.app.activities.room.Notes
import com.notemaker.app.activities.ui.fragments.fragments.CustomDialogFragment
import kotlinx.android.synthetic.main.activity_working_dashboard.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch

class WorkingDashboard : AppCompatActivity() {

    private val db by lazy {
        Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "notes_db"
        ).build()

    }
    var items: MutableList<Notes> = mutableListOf()
    private lateinit var notesRecyclerViewAdapter: NotesRecyclerViewAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_working_dashboard)
        init()

        addButton.setOnClickListener { addNote() }

    }


    private fun init() {

        recyclerView.layoutManager = LinearLayoutManager(this)
        notesRecyclerViewAdapter = NotesRecyclerViewAdapter(items)

        CoroutineScope(Main).launch {
            read()
        }
        recyclerView.adapter = notesRecyclerViewAdapter
        notesRecyclerViewAdapter.notifyDataSetChanged()
        refresh()

    }

    private suspend fun read() {
        for (note in db.notesDao().getAll()) {
            items.add(note)
            d("logNotes", note.title.toString())
        }

    }


    private fun refresh() {

        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = true
            clearRecyclerview()
            Handler(Looper.getMainLooper()).postDelayed({
                swipeRefresh.isRefreshing = false
                CoroutineScope(Main).launch { read() }

            }, 2000)

        }
    }


    private fun clearRecyclerview() {
        items.clear()
    }


    private fun addNote() {
        CustomDialogFragment.newInstance().show(supportFragmentManager, CustomDialogFragment.TAG)

    }


}
