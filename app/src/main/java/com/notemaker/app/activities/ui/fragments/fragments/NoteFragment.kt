package com.notemaker.app.activities.ui.fragments.fragments

import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room

import com.notemaker.app.R
import com.notemaker.app.activities.ui.fragments.activities.WorkingDashboard
import com.notemaker.app.activities.models.NoteItemViewModel
import com.notemaker.app.activities.room.AppDatabase
import com.notemaker.app.activities.room.Notes

import kotlinx.android.synthetic.main.activity_working_dashboard.recyclerView
import kotlinx.android.synthetic.main.fragment_note.view.*
import kotlinx.android.synthetic.main.fragment_note.view.saveButton
import kotlinx.android.synthetic.main.fragment_note.view.titleEditText
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch


class NoteFragment : BaseFragment() {

    private val db by lazy {
        Room.databaseBuilder(
            rootView!!.context,
            AppDatabase::class.java, "notes_db"
        ).build()

    }

    private lateinit var noteItemViewModel: NoteItemViewModel
    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        rootView!!.saveButton.setOnClickListener {
            CoroutineScope(Main).launch {
                save()
            }
        }

        val liveItem = ViewModelProvider(this).get(NoteItemViewModel::class.java)
        liveItem.noteGet().observe(this, Observer {
            val note = it
            d("liveUser?", "$note")
            if (note != null) {
                val activity = activity as WorkingDashboard
                activity.items.add(0, note)
                activity.recyclerView.scrollToPosition(0)
                activity.recyclerView.adapter!!.notifyItemInserted(0)

////                activity.items.add(0, note)
//                activity.recyclerView.scrollToPosition(0)
//                activity.recyclerView.adapter!!.notifyItemInserted(0)

                d("liveUser", "$note")

            }
        })


    }

    override fun getLayoutResource(): Int = R.layout.fragment_note


    private suspend fun save() {

        val titleEditText = rootView!!.titleEditText.text
        val descriptionEditText = rootView!!.descriptionEditText.text
        if (titleEditText.isNotEmpty() && descriptionEditText.isNotEmpty()) {
            val item = Notes()
            item.title = titleEditText.toString()
            item.description = descriptionEditText.toString()
            d("model", "$item.title")
            db.notesDao().insertAll(item)
            for (i in db.notesDao().getAll())
                d("logNote", "${i.title}")
            noteItemViewModel = ViewModelProvider(this).get(NoteItemViewModel::class.java)
            noteItemViewModel.noteSet(item)


        }


    }


}



