package com.notemaker.app.activities.ui.fragments.fragments

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.notemaker.app.R
import com.notemaker.app.activities.App
import com.notemaker.app.activities.models.NoteItemViewModel
import com.notemaker.app.activities.room.AppDatabase
import com.notemaker.app.activities.room.Notes
import com.notemaker.app.activities.ui.fragments.activities.WorkingDashboard
import kotlinx.android.synthetic.main.activity_working_dashboard.*
import kotlinx.android.synthetic.main.fragment_note.*
import kotlinx.android.synthetic.main.fragment_note.view.*
import kotlinx.android.synthetic.main.fragment_note.view.backButton
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CustomDialogFragment : DialogFragment(), View.OnClickListener {
    companion object {
        fun newInstance() = CustomDialogFragment()
        const val TAG = "CREATE_NOTE"
    }

    private val db by lazy {
        Room.databaseBuilder(
            App.instance!!.getContext(),
            AppDatabase::class.java, "notes_db"
        ).build()

    }
    private lateinit var itemView: View
    private lateinit var noteItemViewModel: NoteItemViewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        itemView = inflater.inflate(R.layout.fragment_note, container)

        //listeners
        itemView.saveButton.setOnClickListener(this)
        itemView.backButton.setOnClickListener(this)


        val liveItem = ViewModelProvider(this).get(NoteItemViewModel::class.java)
        liveItem.noteGet().observe(this, Observer {
            val note = it
            Log.d("liveUser?", "$note")
            if (note != null) {
                val activity = activity as WorkingDashboard
                activity.items.add(0, note)
                activity.recyclerView.scrollToPosition(0)
                activity.recyclerView.adapter!!.notifyItemInserted(0)
                Log.d("liveUser", "$note")

            }
        })


        return itemView


    }

    private suspend fun save(){

        val titleEditText = itemView.titleEditText.text
        val descriptionEditText = itemView.descriptionEditText.text
        if (titleEditText.isNotEmpty() && descriptionEditText.isNotEmpty()) {
            val item = Notes()
            item.title = titleEditText.toString()
            item.description = descriptionEditText.toString()
            Log.d("model", "$item.title")
            db.notesDao().insertAll(item)
            for (i in db.notesDao().getAll())
                Log.d("logNote", "${i.title}")
            noteItemViewModel = ViewModelProvider(this).get(NoteItemViewModel::class.java)
            noteItemViewModel.noteSet(item)

            dismiss()

        }


    }

    private fun backButton(){
        dismiss()
    }




    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.setBackgroundDrawable(
            ContextCompat.getDrawable(
                App.instance!!.getContext(),
                android.R.color.transparent
            )
        )
        setStyle(STYLE_NO_FRAME, android.R.style.Theme)
        dialog?.window?.setLayout(
            250,
            250
        )
    }


    override fun onClick(p0: View?) {
        when(p0!!.id){
            R.id.backButton -> backButton()
            R.id.saveButton -> CoroutineScope(Dispatchers.Main).launch { save() }
        }
    }

}