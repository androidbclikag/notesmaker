package com.notemaker.app.activities

import android.app.Application
import android.content.Context

class App : Application() {

    companion object {
        var instance: App? = null
        private lateinit var context: Context


    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        context = applicationContext

    }
    fun getContext() = context




}