package com.notemaker.app.activities.ui.fragments.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.notemaker.app.R
import com.notemaker.app.activities.room.Notes
import com.notemaker.app.databinding.NoteItemLayoutBinding

class NotesRecyclerViewAdapter(
    private val dataSet: MutableList<Notes>
) :
    RecyclerView.Adapter<NotesRecyclerViewAdapter.ViewHolder>() {
    inner class ViewHolder(private val binding: NoteItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        fun onBind() {
            binding.itemModel = dataSet[adapterPosition]

        }


        override fun onClick(v: View?) {

        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.note_item_layout,
            parent,
            false
        )
    )


    override fun getItemCount(): Int = dataSet.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.onBind()
}