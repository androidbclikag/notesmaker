package com.notemaker.app.activities.ui.fragments.adapters

interface CustomOnClick {
    fun onClick(position: Int)
}