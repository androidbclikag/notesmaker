package com.notemaker.app.activities.ui.fragments.fragments

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.notemaker.app.R
import com.notemaker.app.activities.sharedpreferences.UserData
import com.notemaker.app.activities.tools.Tools
import com.notemaker.app.activities.tools.setVisibility
import com.notemaker.app.activities.ui.fragments.activities.AuthenticationActivity
import kotlinx.android.synthetic.main.authentication_activity.*
import kotlinx.android.synthetic.main.fragment_user_name.view.*
import kotlinx.android.synthetic.main.fragment_user_name.view.imageTakingNote


class UserNameFragment : BaseAuthFragment() {
    private lateinit var activityContext: Activity
    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        init()
        rootView!!.imageTakingNote.setVisibility(true)
        rootView!!.userNameCV.setVisibility(true)
        animateViews()


    }


    override fun getLayoutResource(): Int = R.layout.fragment_user_name

    private fun init() {
        activityContext = activity as AuthenticationActivity
        rootView!!.nextButton.setOnClickListener { saveUserName()
        }

    }

    private fun animateViews() {
        Tools.animateView(activityContext, rootView!!.imageTakingNote, R.anim.slide_up)
        Tools.animateView(activityContext, rootView!!.userNameCV, R.anim.slide_up)
    }

    private fun nextToSignUp() {
        val mPager = activityContext.authenticationVP
        if (mPager.currentItem != 0)
            mPager.currentItem = 2

    }

    private fun saveUserName() {

        val userName = rootView!!.userNameET.text.toString()
        if (userName.isNotEmpty()) {
            UserData.instance().saveData("UserName", userName)
            nextToSignUp()
        } else
            rootView!!.userNameLayout.error = getString(R.string.errorUserName)

    }

}