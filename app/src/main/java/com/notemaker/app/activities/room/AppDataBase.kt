package com.notemaker.app.activities.room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [ Notes::class] , version = 3, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun notesDao(): NotesDao

}