package com.notemaker.app.activities.tools

import android.content.Context
import android.view.View
import android.view.animation.AnimationUtils

object Tools {


fun animateView(context: Context, view: View,  id:Int){
    val animation = AnimationUtils.loadAnimation(context, id)
    view.startAnimation(animation)
}
}