package com.notemaker.app.activities.ui.fragments.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast

import com.notemaker.app.R
import com.notemaker.app.activities.tools.Tools
import com.notemaker.app.activities.tools.setVisibility
import com.notemaker.app.activities.ui.fragments.activities.AuthenticationActivity
import com.notemaker.app.activities.ui.fragments.activities.WorkingDashboard
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_sign_up.view.*
import kotlinx.android.synthetic.main.fragment_sign_up.view.imageTakingNote


class SignUpFragment : BaseAuthFragment() {
    private lateinit var auth: FirebaseAuth
    private lateinit var activityContext: AuthenticationActivity

    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        init()
        rootView!!.imageTakingNote.setVisibility(true)
        rootView!!.registerCV.setVisibility(true)

        animateViews()


    }


    private fun init() {

        rootView!!.signUpButton.setOnClickListener {
            signUp()
        }
    }

    private fun animateViews() {
        activityContext = activity as AuthenticationActivity
        Tools.animateView(activityContext, rootView!!.imageTakingNote, R.anim.slide_up)
        Tools.animateView(activityContext, rootView!!.registerCV, R.anim.slide_up)
    }


    override fun getLayoutResource(): Int = R.layout.fragment_sign_up


    private fun signUp() {

        auth = FirebaseAuth.getInstance()
        val password = rootView!!.passwordEditTextSU.text.toString()
        val confirmPassword = rootView!!.confirmPasswordEditTextSU.text.toString()
        val email = rootView!!.emailEditTextSU.text.toString()
        val context = activity as AuthenticationActivity

        if (password == confirmPassword && password.isNotEmpty() && confirmPassword.isNotEmpty() && email.isNotEmpty()) {
            auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(context) { task ->
                    if (task.isSuccessful) {
                        val intent = Intent(
                            context,
                            WorkingDashboard::class.java
                        )
                        startActivity(intent)
                        context.finish()
                        d("", "createUserWithEmail:success")
                    } else {
                        Log.w("", "createUserWithEmail:failure", task.exception)
                        Toast.makeText(
                            context, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()

                    }

                }
        } else if (password != confirmPassword) {
            rootView!!.confirmPasswordLayout.error = getString(R.string.confirm_password_error)


        } else {
            Toast.makeText(
                context, "please confirm password correctly",
                Toast.LENGTH_SHORT
            ).show()
        }

    }

}
