package com.notemaker.app.activities.ui.fragments.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.notemaker.app.R
import com.notemaker.app.activities.ui.fragments.adapters.AuthenticationViewPagerAdapter
import com.notemaker.app.activities.ui.fragments.fragments.SignInFragment
import com.notemaker.app.activities.ui.fragments.fragments.SignUpFragment
import com.notemaker.app.activities.ui.fragments.fragments.UserNameFragment
import kotlinx.android.synthetic.main.authentication_activity.*

class AuthenticationActivity : AppCompatActivity() {
    private lateinit var authenticationViewPager: AuthenticationViewPagerAdapter
    private var fragmentsList = mutableListOf<Fragment>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.authentication_activity)
        init()

    }

    private fun init() {
        authenticationViewPager =
            AuthenticationViewPagerAdapter(supportFragmentManager, fragmentsList)
        fragmentsList.add(SignInFragment())
        fragmentsList.add(UserNameFragment())
        fragmentsList.add(SignUpFragment())
        authenticationVP.adapter = authenticationViewPager
        authenticationViewPager.notifyDataSetChanged()


    }









}
