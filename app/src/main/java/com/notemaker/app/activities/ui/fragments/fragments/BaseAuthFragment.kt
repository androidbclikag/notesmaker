package com.notemaker.app.activities.ui.fragments.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


abstract class BaseAuthFragment : Fragment() {

    var rootView: View? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (rootView == null)
            rootView = inflater.inflate(getLayoutResource(), container, false)
        start(inflater, container, savedInstanceState)
        return rootView


    }

    abstract fun start(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    )


    abstract fun getLayoutResource(): Int

}