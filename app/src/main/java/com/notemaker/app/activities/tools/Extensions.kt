package com.notemaker.app.activities.tools

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE

fun View.setVisibility(visible: Boolean){
    visibility = if(visible)
        VISIBLE
    else
        GONE
}