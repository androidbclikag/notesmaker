package com.notemaker.app.activities.ui.fragments.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.notemaker.app.R
import com.notemaker.app.activities.tools.Tools
import kotlinx.android.synthetic.main.splash_activity.*

class SplashActivity : AppCompatActivity() {
    companion object {
        const val SPLASH_TIME_OUT_LONG: Long = 3000
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)
        animateNote()
    }

    override fun onStart() {
        super.onStart()

        Handler(Looper.getMainLooper()).postDelayed(
            {
                startMainActivity() },
            SPLASH_TIME_OUT_LONG
        )

    }

    override fun onPause() {
        super.onPause()
        Handler(Looper.getMainLooper()).removeCallbacks(
            { startMainActivity() },
            SPLASH_TIME_OUT_LONG
        )
    }


    private fun startMainActivity() {
        val intent = Intent(this, AuthenticationActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }
    private fun animateNote(){
        Tools.animateView(this, splashTV0, R.anim.fade_in)
        Tools.animateView(this, splashTV1, R.anim.fade_in)
//        val animation = AnimationUtils.loadAnimation(this, R.anim.fade_in)
//        splashTV0.startAnimation(animation)
//        splashTV1.startAnimation(animation)
    }

}
