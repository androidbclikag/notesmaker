package com.notemaker.app.activities.models

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.notemaker.app.activities.room.Notes


class NoteItemViewModel : ViewModel() {

    private val liveUser: MutableLiveData<Notes> by lazy {
        MutableLiveData<Notes>()
    }

    fun noteSet(notes: Notes) {
        liveUser.postValue(notes)
    }

    fun noteGet(): MutableLiveData<Notes> {
        return liveUser
    }


}